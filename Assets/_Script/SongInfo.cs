﻿using UnityEngine;

[CreateAssetMenu(menuName = "Custom/Song Data")] public class SongInfo : ScriptableObject {
    public AudioClip song;
    public int bpm;
    public string title;
}
