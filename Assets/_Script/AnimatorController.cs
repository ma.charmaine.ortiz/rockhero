﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimatorController : MonoBehaviour
{
    [SerializeField]
    GameObject nextCanvas;
    public void CanvasTriggerAnimation(string trigger)
    {
        nextCanvas.SetActive(true);
        Animator canvasAnimator = nextCanvas.GetComponent<Animator>();
        canvasAnimator.SetTrigger(trigger);
        this.gameObject.SetActive(false);

    }
}
