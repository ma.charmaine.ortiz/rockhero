﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Option : MonoBehaviour
{
    [SerializeField]
    private Toggle sound;

    [SerializeField]
    private Toggle easy;

    [SerializeField]
    private Toggle normal;

    [SerializeField]
    private Toggle hard;

    [SerializeField]
    private GameObject gameCanvas;


    GameControl command;

    public void PlayGame()
    {
        if (easy.isOn || normal.isOn || hard.isOn)
        {
            GameObject.Find("BG&TitleCanvas").GetComponent<Animator>().SetTrigger("Close");
            this.GetComponent<Animator>().SetTrigger("Close");
        }

    }
    public void OpenGameCanvas()
    {
        gameCanvas.SetActive(true);
        command = GameObject.Find("GameSettings").GetComponent<GameControl>();
        command.StartGame();
        GameObject.Find("GameCanvas").GetComponent<Animator>().SetTrigger("Open");
        if (easy.isOn)
        {
            command.SetSpeed("easy");
        }
        else if (normal.isOn)
        {
            command.SetSpeed("normal");
        }
        else if (hard.isOn)
        {
            command.SetSpeed("hard");
        }
        this.gameObject.SetActive(false);

        if (!sound.isOn)
        {
            GameObject.Find("SongController").GetComponent<SongController>().StopSong();
        }
    }
}
