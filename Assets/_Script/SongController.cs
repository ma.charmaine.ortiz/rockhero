﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SongController : MonoBehaviour
{
    [SerializeField]
    private SongInfo[] song;
    AudioSource source;
    float timeDuration;
    int bpm;
    string title;
    int index;
    private void Start()
    {
        source = this.GetComponent<AudioSource>();
    }
    public void StartSong()
    {
        index = Random.Range(0, song.Length - 1);
        source.clip = song[index].song;
        source.Play();
    }

    public float getTime()
    {
        timeDuration = source.clip.length;
        return timeDuration;
    }
    public int getBPM()
    {
        bpm = song[index].bpm;
        return bpm;
    }
    public string getTitle()
    {
        title = song[index].title;
        return title;
    }
    public void StopSong()
    {
        source.Stop();
    }
    
}
