﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{
    int speed;

    GameObject target;
    // Start is called before the first frame update
    void Start()
    {
        target = GameObject.Find("Target");
        speed = GameObject.Find("GameSettings").GetComponent<GameControl>().getBallSpeed();
    }
    // Update is called once per frame
    void Update()
    {
        
        float step = speed * Time.deltaTime; // calculate distance to move
        this.transform.position = Vector3.MoveTowards(transform.position, new Vector3(transform.position.x, target.transform.position.y), step);
        
    }
}
