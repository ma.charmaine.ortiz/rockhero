﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    [SerializeField]
    Transform leftParent;

    [SerializeField]
    Transform rightParent;

    [SerializeField]
    GameObject lPrefab;

    [SerializeField]
    GameObject rPrefab;

    float speed;


    public bool isSpawning;

    // Start is called before the first frame update
    void Start()
    {
        speed = GameObject.Find("GameSettings").GetComponent<GameControl>().getSpawningSpeed();
        speed = (speed / 4)/15;
        isSpawning = true;
        StartCoroutine(SpawnLoop());
    }
    IEnumerator SpawnLoop()
    {
        //Pause Before Start
        yield return new WaitForSeconds(1.5f);

        //Start Spawn
        while (isSpawning)
        {
            yield return new WaitForSeconds(speed);
            int randomNumber = Random.Range(0, 3);
            Spawn(randomNumber);
        }

    }
    // Spawn Ball
    void Spawn(int randomNumber)
    {
        if (randomNumber == 0)
        {
            GameObject leftBall = Instantiate(lPrefab, leftParent);
            leftBall.transform.SetParent(leftParent);
        }
        if (randomNumber == 1)
        {
            GameObject rightBall = Instantiate(rPrefab, rightParent);
            rightBall.transform.SetParent(rightParent);
        }
        if (randomNumber == 2)
        {
            GameObject leftBall = Instantiate(lPrefab, leftParent);
            leftBall.transform.SetParent(leftParent);
            GameObject rightBall = Instantiate(rPrefab, rightParent);
            rightBall.transform.SetParent(rightParent);
        }
    }
    public void StartGame()
    {
        StartCoroutine(SpawnLoop());
        isSpawning = true;
    }
    public void StopSpawnGameOver()
    {
        isSpawning = false;
        StopCoroutine(SpawnLoop());
        Destroy(this.gameObject);
    }
    public void StopSpawnTimesUp()
    {
        isSpawning = false;
        StopCoroutine(SpawnLoop());
    }
}
