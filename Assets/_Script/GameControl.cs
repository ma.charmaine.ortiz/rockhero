﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameControl : MonoBehaviour
{
    private int score;
    private float life;
    private Spawner spawner;
    private SongController song;
    private Text scoreText;
    private Text timeText;
    private Image lifeValue;
    private float timer;
    private int bpm;
    private int spawningSpeed;
    private int ballspeed;
    private string difficultyLevel;
    private bool start;

    [Header("Settings")]
    [SerializeField]
    [Tooltip("Points per right tap")]
    private int scoringPoints;
    [SerializeField]
    [Tooltip("Minus 1 life for every miss")]
    private int lifeSpan;


    [Header("Spawning Speed")]
    [Tooltip("bpm divide to the value of the speed.")]
    [SerializeField]
    private int easySpawning;
    [SerializeField]
    private int normalSpawning;
    [SerializeField]
    private int hardSpawning;


    [Header("Ball Speed")]
    [Tooltip("bpm multiply to the value of the speed.")]
    [SerializeField]
    private int easySpeed;
    [SerializeField]
    private int normalSpeed;
    [SerializeField]
    private int hardSpeed;



    public void StartGame()
    {
        score = 0;
        start = true;


        life = lifeSpan;
        lifeValue = GameObject.Find("Life").GetComponent<Image>();
        life = 1 / life;
        lifeValue.fillAmount = 1;

        OpenSpawner();

        song = GameObject.Find("SongController").GetComponent<SongController>();
        song.StartSong();

        //Get Song Info
        timer = song.getTime();
        bpm = song.getBPM();
        GameObject.Find("SongTitle").GetComponent<Text>().text = song.getTitle();

        scoreText = GameObject.Find("ScoreText").GetComponent<Text>();
        scoreText.text = "0";

        timeText = GameObject.Find("TimeText").GetComponent<Text>();
        
    }

    private void OpenSpawner()
    {
        GameObject.Find("SpawnerController").GetComponent<SpawnerController>().CreateSpawner();
        spawner = GameObject.Find("Spawner").GetComponent<Spawner>();
    }
    public void SetSpeed(string difficulty)
    {
        if (difficulty=="easy")
        {
            spawningSpeed = bpm/easySpawning;
            ballspeed =bpm * easySpeed;

        }
        else if(difficulty == "normal")
        {
            spawningSpeed = bpm / normalSpawning;
            ballspeed = bpm * normalSpeed;
        }
        else if (difficulty == "hard")
        {
            spawningSpeed = bpm / hardSpawning;
            ballspeed = bpm * hardSpeed;
        }

    }


    public int getSpawningSpeed()
    {
        return spawningSpeed;
    }

    public int getBallSpeed()
    {
        return ballspeed;
    }

    void Update()
    {
        if (timer > 0 && start)
        {
            timer -= Time.deltaTime;

            string minutes = Mathf.Floor(timer / 60).ToString("00");
            string seconds = (timer % 60).ToString("00");

            timeText.text = string.Format("{0}:{1}", minutes, seconds);
            if(timer <0)
            {
                timeText.text = string.Format("{0}:{1}", "00", "00");
                spawner.StopSpawnGameOver();
                GameObject.Find("GameCanvas").GetComponent<Animator>().SetTrigger("Close");
                GameObject.Find("BG&TitleCanvas").GetComponent<Animator>().SetTrigger("Open");
                
            }
        }
        

    }

    public void Scored()
    {
        score += scoringPoints;
        scoreText.text = score.ToString();
    }

    public void DecreasedLife()
    {
        if(lifeValue.fillAmount > 0)
        {
            lifeValue.fillAmount -= life;
            if (lifeValue.fillAmount <= 0)
            {
                spawner.StopSpawnGameOver();
                song.StopSong();
                start = false;
                GameObject.Find("GameCanvas").GetComponent<Animator>().SetTrigger("Close");
                GameObject.Find("BG&TitleCanvas").GetComponent<Animator>().SetTrigger("Open");
            }
        }
    }

    public int FinalScore()
    {
        return score;
    }
}
