﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EndingController : MonoBehaviour
{
    [SerializeField]
    private GameObject gameCanvas;

    [SerializeField]
    private Toggle sound;

    GameControl command;

    public void Replay()
    {
        GameObject.Find("BG&TitleCanvas").GetComponent<Animator>().SetTrigger("Close");
        this.GetComponent<Animator>().SetTrigger("Close");
    }

    public void OpenGameCanvas()
    {
        gameCanvas.SetActive(true);
        command = GameObject.Find("GameSettings").GetComponent<GameControl>();
        command.StartGame();
        GameObject.Find("GameCanvas").GetComponent<Animator>().SetTrigger("Open");
        
        if (!sound.isOn)
        {
            GameObject.Find("SongController").GetComponent<SongController>().StopSong();
        }
    }

    public void getFinalScore()
    {
        GameObject.Find("FinalScoreText").GetComponent<Text>().text = GameObject.Find("GameSettings").GetComponent<GameControl>().FinalScore().ToString();
    }
}
