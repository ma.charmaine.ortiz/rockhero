﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerController : MonoBehaviour
{
    [SerializeField]
    GameObject spawnerPrefab;
    public void CreateSpawner()
    {
        GameObject spawner = Instantiate(spawnerPrefab, this.gameObject.transform);
        spawner.name = "Spawner";
        spawner.transform.SetParent(this.gameObject.transform);
    }
}
