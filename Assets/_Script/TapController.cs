﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TapController : MonoBehaviour
{
    // Start is called before the first frame update
    bool isBallInside;
    GameObject ballEntered;
    GameControl command;
    Animator rightTrack;
    Animator leftTrack;
    Animator animatorEffects;
    AudioSource[] soundEffects;
    void Start()
    {
        command = GameObject.Find("GameSettings").GetComponent<GameControl>();
        leftTrack = GameObject.Find("LeftTrack").GetComponent<Animator>();
        rightTrack = GameObject.Find("RightTrack").GetComponent<Animator>();
        animatorEffects = GetComponent<Animator>();
        soundEffects = this.GetComponents<AudioSource>();

    }

    // Update is called once per frame
    
    public void Tap()
    {
        //Do some animation before destrying
        if (isBallInside)
        {
            command.Scored();
            TrackEffect("Hit");
            Destroy(ballEntered);
            soundEffects[0].Play();
            animatorEffects.SetTrigger("Hit");
        }
        else if(!isBallInside)
        {

            TrackEffect("Miss");
            command.DecreasedLife();
            soundEffects[1].Play();
            animatorEffects.SetTrigger("Miss");
        }
            
    }

    private void TrackEffect(string trigger)
    {
        if (this.gameObject.name == "LeftRing")
        {
            leftTrack.SetTrigger(trigger);
        }
        else
        {
            rightTrack.SetTrigger(trigger);
        }

    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Ball")
        {
            BallEnteredRing(collision.gameObject);
        }
    }
    private void OnTriggerStay2D(Collider2D collision)
    {
        if(collision.tag == "Ball")
        {
            BallEnteredRing(collision.gameObject);
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Ball")
        {
            Missed(collision.gameObject);
        }
    }
    private void BallEnteredRing(GameObject ball)
    {
        isBallInside = true;
        ballEntered = ball.gameObject;
    }
    private void Missed(GameObject ball)
    {
        isBallInside = false;
        ballEntered = null;
        Destroy(ball, .5f);
        //command.DecreasedLife();
    }
}
